<?php 
//include '../../../Src/Seip131649/Gender/Gender.php';
include_once '../../../vendor/autoload.php';

use App\Seip131649\Gender\Gender;

 $obj = new Gender(); 
 $data = $obj->showOne($_GET['id']);
?>

<fieldset>
    <legend>Update Gender</legend>
   
    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $data['id']?>">
        <label>Enter your Name </label>
        <input type="text" name="title" value="<?php echo $data['title']?>">
        <input type="radio" name="gender" value="Male">
        <label>Male</label>
        <input type="radio" name="gender" value="Female">
        <label>Female</label>
        <input type="submit" name="edit" value="Update">        
    </form>
</fieldset>
