<?php
//include_once '../../../Src/Seip131649/Mobile/Mobile.php';

include_once '../../../vendor/autoload.php';
use App\Seip131649\Mobile\Mobile;

$obj= new Mobile();
$data = $obj->show($_GET['id']);

?>
<fieldset>
    <legend>Update Your Favorite Mobile Model</legend>
   
    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $data['id']?>">
        <label>Enter your Favorite Mobile Model: </label>
        <input type="text" name="title" value="<?php echo $data['title']?>">
        <input type="submit" name="edit" value="Update">        
    </form>
</fieldset>
